function getLargestPrimeFactor(numberToCheck) {
  function isPrime(numberToCheck) {
    let result = true;
    for(let i = 2; i <=(numberToCheck/i); i++) {
      if(numberToCheck%i == 0) { //if numberToCheck is divisible by number other than itself...
        result = false; //return false
      }
    }
    return result;
  }
  
  function getFactors(numberToCheck) {
    let result = [];
    for(let i = 2; i < Math.round(Math.sqrt(numberToCheck)); i++) {
      if(numberToCheck%i == 0) {
        result.push(i);
      }
    }
    return result;
  }
  
  return getFactors(numberToCheck).filter(number => isPrime(number)).reduce((numOne, numTwo) => numOne > numTwo? numOne : numTwo);
}

module.exports.getLargestPrimeFactor = getLargestPrimeFactor;