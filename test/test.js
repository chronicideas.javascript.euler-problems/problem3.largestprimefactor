const assert = require('assert');
const index = require('../index.js');

describe('Problem3', function() {
  it('The largest prime factor returned for the number 600851475143 is correct', function() {
    assert.equal(index.getLargestPrimeFactor(600851475143), 6857);
  });
});

